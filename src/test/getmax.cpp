/****************************************************************/
/*								*/
/*  This is a test program for the CMEM_RCC driver & library	*/
/*								*/
/*  11. Jun. 02  MAJO  created					*/
/*								*/
/***********C 2002 - A nickel program worth a dime***************/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"


int main(void)
{
  u_int dblevel = 0, dbpackage = DFDB_CMEMRCC;
  u_int cnt, ret;
  int handle;
  char dummy[20], name[CMEM_MAX_NAME];
  u_long asize, tsize, size = 0x100000, pa, pa2;

  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
    
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  printf("Enter the size [in bytes] of the buffers to be allocated: ");
  size = gethexd(size);

  sprintf(name,"tseg");
  ret = CMEM_SegmentAllocate(size, name, &handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ret = CMEM_SegmentSize(handle, &asize);
  if (ret)
    rcc_error_print(stdout, ret);

  printf("Actual size of first buffer = 0x%016lx\n", asize); 
  ret = CMEM_SegmentPhysicalAddress(handle, &pa);
  if (ret)
    rcc_error_print(stdout, ret);
  printf("Physical address of first buffer = 0x%16lx\n", pa);
  
  cnt = 0;
  tsize = 0;
  while(1)
  {
    sprintf(name, "tseg%d", cnt++);
    ret = CMEM_SegmentAllocate(size, name, &handle);
    if (ret)
      break;
    
    pa2 = pa;
    ret = CMEM_SegmentPhysicalAddress(handle, &pa);
    if (ret)
      rcc_error_print(stdout, ret);
    printf("Physical address of buffer = 0x%16lx\n", pa);
    
    ret = CMEM_SegmentSize(handle, &asize);
    if (ret)
      rcc_error_print(stdout, ret);
    
    if (pa != pa2 + asize)
    {
      printf("Discontinuous memory. Buffer size = 0x%016lx = %f MB\n", tsize, (float)tsize/1024.0/1024.0);
      tsize = asize;
    }
    else
      tsize += asize;
  }
  rcc_error_print(stdout, ret);
  printf("Press <return> to release the buffers\n");
  fgets(dummy, 20, stdin);

  ret = CMEM_Close();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  return(0);
}
