/****************************************************************/
/*								*/
/*  This is the test program for the CMEM_RCC driver & library	*/
/*								*/
/*  12. Dec. 01  MAJO  created					*/
/*								*/
/***********C 2001 - A nickel program worth a dime***************/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"


//Globals
int cont;

// Prototypes
int stresstest(int mode);
int stresstest2(void);
int zonetest(void);
void SigQuitHandler(int signum);


/*********************************/
void SigQuitHandler(int /*signum*/)
/*********************************/
{
  cont=0;
}


/************/
int main(void)
/************/
{
  u_long asize, paddr, uaddr, size = 0x1000;
  u_int ret, numa_id = 0, dblevel = 20, dbpackage = DFDB_CMEMRCC;
  int stat, fun, handle;
  char name[CMEM_MAX_NAME];
  static struct sigaction sa;
  cmem_rcc_t desc;
  
  sigemptyset(&sa.sa_mask); 
  sa.sa_flags = 0; 
  sa.sa_handler = SigQuitHandler;
  stat = sigaction(SIGQUIT, &sa, NULL);
  if (stat < 0)
  {
    printf("Cannot install signal handler (error=%d)\n", stat);
    exit(0);
  }
  
  fun = 18;
    
  while (fun != 0)  
  {
    printf("\n");
    printf("Select an option:\n");
    printf("   1 CMEM_Open                                2 CMEM_Close\n");  
    printf("   3 CMEM_SegmentAllocate                     4 CMEM_SegmentAllocateNuma\n");
    printf("   5 CMEM_BPASegmentAllocate                  6 CMEM_NumaSegmentAllocate\n");
    printf("   7 CMEM_SegmentFree                         8 CMEM_SegmentGet\n");
    printf("   9 CMEM_SegmentSize                        10 CMEM_SegmentPhysicalAddress\n");
    printf("  11 CMEM_Dump                               12 CMEM_SegmentVirtualAddress\n");
    printf("  13 CMEM_SegmentLock                        14 CMEM_SegmentUnlock\n");
    printf("  15 CMEM_OpenNoPage                         16 CMEM_SegmentUnlockAndFree\n");
    printf("==========================================================================\n");
    printf("  17 Stress test dynamic                     18 Stress test pre-allocated\n");
    printf("  19 Stress test both                        20 Set debug parameters\n");
    printf("  21 Dump memory                             22 Initialize buffer\n");
    printf("  23 /proc test                              24 gfpbpa_zone test\n");
    printf("   0 Exit\n");
    printf("Your choice ");
    fun = getdecd(fun);
    if (fun == 1)
    {  
      ret = CMEM_Open();
      if (ret)
        rcc_error_print(stdout, ret);
    }
    
    if (fun == 2) 
    {
      ret = CMEM_Close();
      if (ret)
        rcc_error_print(stdout, ret);
    }    
    
    if (fun == 3) 
    {
      printf("Enter the size [in bytes] of the segment to be allocated: ");
      size = gethexd(size);

      printf("Enter the name of the segment: ");
      getstrd(name, (char *)"cmem_rcc_test");

      ret = CMEM_SegmentAllocate(size, name, &handle);
      if (ret)
        rcc_error_print(stdout, ret);
      printf("The handle is %d\n", handle);
    }    
    
    if (fun == 4) 
    {
      printf("Enter the size [in bytes] of the segment to be allocated: ");
      size = gethexd(size);

      printf("Enter the name of the segment: ");
      getstrd(name, (char *)"cmem_rcc_test");
      
      printf("Enter the NUMA ID: ");
      numa_id = getdecd(numa_id);
      
      ret = CMEM_SegmentAllocateNuma(size, numa_id, name, &handle);   
      if (ret)
        rcc_error_print(stdout, ret);
	
      printf("The handle is %d\n", handle);
    }  
    
    if (fun == 5) 
    {
      printf("Enter the size [in bytes] of the segment to be allocated: ");
      size = gethexd(size);

      printf("Enter the name of the segment: ");
      getstrd(name, (char *)"cmem_rcc_bpa_test");

      printf("Requesting segment of %016lx bytes\n", size);

      ret = CMEM_BPASegmentAllocate(size, name, &handle);
      if (ret)
        rcc_error_print(stdout, ret);
      printf("The handle is %d\n", handle);
    }  
    
    if (fun == 6) 
    {      
      printf("Enter the size [in bytes] of the segment to be allocated: ");
      size = gethexd(size);

      printf("Enter the name of the segment: ");
      getstrd(name, (char *)"cmem_rcc_bpa_test");

      printf("Enter the NUMA ID: ");
      numa_id = getdecd(numa_id);

      ret = CMEM_NumaSegmentAllocate(size, numa_id, name, &handle);
      if (ret)
        rcc_error_print(stdout, ret);

      printf("The handle is %d\n", handle);
    }
       
    if (fun == 7) 
    {
      printf("Enter the handle: ");
      handle = getdecd(handle);
      
      ret = CMEM_SegmentFree(handle);
      if (ret)
        rcc_error_print(stdout, ret);
      printf("handle %d returned\n", handle);
    }    
    
    if (fun == 8)
    {
      printf("Enter the handle: ");
      handle = getdecd(handle);
      ret = CMEM_SegmentGet(handle ,&desc);
       if (ret)
        rcc_error_print(stdout, ret); 
      printf("desc.type   = %d\n", desc.type);	     
      printf("desc.paddr  = 0x%016lx\n", desc.paddr);	     
      printf("desc.kaddr  = 0x%016lx\n", desc.kaddr);	     
      printf("desc.size   = 0x%016lx\n", desc.size);	     
      printf("desc.order  = %d\n", desc.order);	     
      printf("desc.locked = %d\n", desc.locked);	     
      printf("desc.name   = %s\n", desc.name);	     
    }
    
    if (fun == 9) 
    { 
      printf("Enter the handle: ");
      handle = getdecd(handle);
      ret = CMEM_SegmentSize(handle, &asize);
      if (ret)
        rcc_error_print(stdout, ret);
      printf("Segment size = 0x%016lx bytes\n", asize);
    }
    
    if (fun == 10) 
    {
      printf("Enter the handle: ");
      handle = getdecd(handle);
      ret = CMEM_SegmentPhysicalAddress(handle, &paddr);
      if (ret)
        rcc_error_print(stdout, ret); 
      printf("Segment physical address = 0x%016lx\n", paddr);
    }

    if (fun == 11) 
      CMEM_Dump();
      
    if (fun == 12) 
    {
      printf("Enter the handle: ");
      handle = getdecd(handle);  
      ret = CMEM_SegmentVirtualAddress(handle, &uaddr);
      if (ret)
        rcc_error_print(stdout, ret);
      printf("Segment virtual address = 0x%016lx\n", uaddr);
    }
    
    if (fun == 13)
    {
      printf("Enter the handle: ");
      handle = getdecd(handle);
      ret = CMEM_SegmentLock(handle);
      if (ret)
        rcc_error_print(stdout, ret); 
      printf("The segment has been locked\n");
    }
    
    if (fun == 14)
    {
      printf("Enter the handle: ");
      handle = getdecd(handle);
      ret = CMEM_SegmentUnlock(handle);
      if (ret)
        rcc_error_print(stdout, ret); 
      printf("The segment has been unlocked\n");
    }

    if (fun == 15)
    {  
      ret = CMEM_OpenNopage();
      if (ret)
        rcc_error_print(stdout, ret);
    }


    if (fun == 16)
    {
      printf("Enter the name of the segment: ");
      getstrd(name, (char *)"cmem_rcc_test");
      ret = CMEM_SegmentUnlockAndFree(name);
      if (ret)
        rcc_error_print(stdout, ret);
    }
    
    if (fun == 17)
      stresstest(1); 

    if (fun == 18) 
      stresstest(2);

    if (fun == 19) 
      stresstest(3);

    if (fun == 20)
    { 
      printf("Enter the debug level: ");
      dblevel = getdecd(dblevel);
      printf("Enter the debug package: ");
      dbpackage = getdecd(dbpackage);
      DF::GlobalDebugSettings::setup(dblevel, dbpackage);
    }
    
    if (fun == 21)
    {
      u_long mem_phys = 0, mem_size = 10, mem_virt;
      u_int loop, alignment = 0xfff, offset;
      int cmem_fd;
      u_char *cptr;
      
      printf("Enter the physical address: ");
      mem_phys = gethexd(mem_phys);
      printf("Enter the number of bytes to be dumped: ");
      mem_size = gethexd(mem_size);
  
      if ((cmem_fd = open("/dev/cmem_rcc", O_RDWR)) < 0)
        printf("Failed to open /dev/cmem_rcc\n");
      else
      {
        if (mem_phys & alignment) //Not 4K alligned
        {
	  offset = mem_phys & alignment;
	  mem_phys = mem_phys & ~alignment;
	  printf("offset = 0x%04x\n", offset);
	  printf("mem_phys = 0x%016lx\n", mem_phys);
	}   
	else
	  offset = 0;
	  
        mem_virt = (u_long)mmap(0, mem_size, PROT_READ|PROT_WRITE, MAP_SHARED, cmem_fd, (u_long)mem_phys);
	printf("mem_virt = 0x%016lx\n", mem_virt);
	cptr = (u_char *)(mem_virt + offset);
	for (loop = 0; loop < mem_size; loop++)
	  printf("Byte %d = 0x%02x\n", loop, *cptr++);
      }
    }
    
    if (fun == 22) 
    {
      u_int loop;
      u_char *cptr;

      printf("Enter the handle: ");
      handle = getdecd(handle);  
      ret = CMEM_SegmentVirtualAddress(handle, &uaddr);
      if (ret)
        rcc_error_print(stdout, ret);
      printf("Segment virtual address = 0x%016lx\n", uaddr);
      cptr = (u_char *)uaddr;
      for (loop = 0; loop < 50; loop++)
      {
	*cptr = loop;
	printf("Byte %d set to 0x%02x\n", loop, *cptr);
	cptr++;
      }
    }

    if (fun == 23) 
      stresstest2();

    if (fun == 24) 
      zonetest();
  }

  return(0);
}


/**********************/
int stresstest(int mode)
/**********************/
{
  int buff_ok = 0, buff_ret_ok = 0, handle1[1000], handle2[1000];
  u_int n1, buffs, num, ret, loop;
  u_long idummy, size, dummy;
       
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }      
  
  printf("Enter the number of loops (0=run forever) \n");
  num = getdecd(100000);

  printf("Enter the number of concurrent buffers (max. 1000) \n");
  buffs  = getdecd(10);
  
  cont = 1;
  n1 = 0;
  printf("Test running. Press <ctrl+\\> to stop\n");
  while(cont)
  { 
    for(loop = 0; loop < buffs; loop++)
    {
      size = rand() % 20000;
      if (size == 0)
        size = 100;

      if (mode == 1 || mode == 3)
      {
        ret = CMEM_SegmentAllocate(size, (char *)"cmem_rcc_stress1", &handle1[loop]);
        if (ret) { rcc_error_print(stdout, ret); printf("buff_ok = %d\n", buff_ok); exit(-1); } 

        ret = CMEM_SegmentSize(handle1[loop], &idummy);
        if (ret) { rcc_error_print(stdout, ret); printf("buff_ok = %d\n", buff_ok); exit(-1); }       

        ret = CMEM_SegmentVirtualAddress(handle1[loop], &dummy);
        if (ret) { ;rcc_error_print(stdout, ret); printf("buff_ok = %d\n", buff_ok); exit(-1); }      

        ret = CMEM_SegmentPhysicalAddress(handle1[loop], &dummy);     
        if (ret) { rcc_error_print(stdout, ret); printf("buff_ok = %d\n", buff_ok); exit(-1); }    
      }
      
      if (mode == 2 || mode == 3)
      {
        ret = CMEM_BPASegmentAllocate(size, (char *)"cmem_rcc_stress2", &handle2[loop]);
        if (ret) { rcc_error_print(stdout, ret); printf("buff_ok = %d\n", buff_ok); exit(-1); } 
	
        ret = CMEM_SegmentSize(handle2[loop], &idummy);
        if (ret) { rcc_error_print(stdout, ret); printf("buff_ok = %d\n", buff_ok); exit(-1); }       
	
        ret = CMEM_SegmentVirtualAddress(handle2[loop], &dummy);
        if (ret) { rcc_error_print(stdout, ret); printf("buff_ok = %d\n", buff_ok); exit(-1); }      
	
        ret = CMEM_SegmentPhysicalAddress(handle2[loop], &dummy);     
        if (ret) { rcc_error_print(stdout, ret); printf("buff_ok = %d\n", buff_ok); exit(-1); }    	
      }
      
      buff_ok++;
    }
 
    for(loop = 0; loop < buffs; loop++)
    {      
      if (mode == 1 || mode == 3)
      {
        printf("calling CMEM_SegmentFree 1 for handle %d\n", handle1[loop]);
        ret = CMEM_SegmentFree(handle1[loop]);
      }
      
      if (mode == 2 || mode == 3)
      {
        printf("calling CMEM_SegmentFree 2 for handle %d\n", handle2[loop]);
        ret = CMEM_SegmentFree(handle2[loop]);
      }

      if (ret) 
      { 
        rcc_error_print(stdout, ret); 
        printf("buff_ret_ok = %d\n", buff_ret_ok); 
	exit(-1); 
      } 
      buff_ret_ok++;
    }
    
    
    if (num > 0)
    {
      num--;
      if ((num & 0xfff) == 0)
	printf("Still %d runs through the loop to go\n", num);  
    }
    
    if (num == 0)
      break;  
    n1++;
  }
 
  printf("%d loops executed\n", n1); 
  CMEM_Dump();

  ret = CMEM_Close();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  return(0);
}


/*******************/
int stresstest2(void)
/*******************/
{
  u_int buffs, ret, loop;
  int handle1[1000];
  char dummy[20];
  
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }      

  printf("Enter the number of concurrent buffers (max. 1000) \n");
  buffs  = getdecd(10);
  
  for(loop = 0; loop < buffs; loop++)
  {
    ret = CMEM_SegmentAllocate(100, (char *)"cmem_rcc_stress", &handle1[loop]);
    if (ret) 
    { 
      rcc_error_print(stdout, ret); 
      return(-1); 
    }       
  }
  
  printf("Press <return> to release the buffers\n");
  fgets(dummy, 10, stdin);
 
  for(loop = 0; loop < buffs; loop++)
  {
    ret = CMEM_SegmentFree(handle1[loop]);
    if (ret) 
    { 
      rcc_error_print(stdout, ret); 
      return(-1); 
    } 
  }
    
  ret = CMEM_Close();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  return(0);
}


/****************/
int zonetest(void)
/****************/
{
  u_int ret, loop;
  int handle;
  u_long paddr;
  
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }      

  for (loop = 1; loop < 10000; loop++)
  {
    ret = CMEM_SegmentAllocate(100 * loop, (char *)"cmem_rcc_stress", &handle);
    if (ret) 
    { 
      rcc_error_print(stdout, ret); 
      return(-1); 
    }       

    ret = CMEM_SegmentPhysicalAddress(handle, &paddr);
    if (ret)
      rcc_error_print(stdout, ret); 
    printf("Segment physical address = 0x%016lx\n", paddr);
    if (paddr >> 32)
    {
      printf("ERROR. Address 0x%16lx is not in the 32bit address space\n", paddr);
      return-(1);
    }

    ret = CMEM_SegmentFree(handle);
    if (ret) 
    { 
      rcc_error_print(stdout, ret); 
      return(-1); 
    } 
    }

    
  ret = CMEM_Close();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    return(-1);
  }

  return(0);
}


