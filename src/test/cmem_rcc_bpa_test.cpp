/****************************************************************/
/*								*/
/*  This is a test program for the CMEM_RCC driver & library	*/
/*								*/
/*  12. Dec. 01  MAJO  created					*/
/*								*/
/***********C 2001 - A nickel program worth a dime***************/


#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"

int main(void)
{
  u_long paddr, uaddr, asize, size1 = 0x1000, size2 = 0x10000;
  u_int mtype = TYPE_BPA, ret, loop;
  u_int dblevel = 0, dbpackage = DFDB_CMEMRCC, *ptr;
  int handle2, handle;
  char dummy[20], name1[CMEM_MAX_NAME], name2[CMEM_MAX_NAME];
    
  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);  
  
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  printf("Testing BPA based memory allocation\n");
  printf("Enter the memory type (%d = Default BPA, %d = GFPBPA): ", TYPE_BPA, TYPE_GFPBPA);
  mtype = getdecd(mtype);
  printf("Enter the size [in bytes] of the first buffer: ");
  size1 = gethexd(size1);
  printf("Enter the name of the first buffer: ");
  getstrd(name1, (char *)"cmem_rcc_test_1");

  printf("Enter the size [in bytes] of the second buffer: ");
  size2 = gethexd(size2);
  printf("Enter the name of the second buffer: ");
  getstrd(name2, (char *)"cmem_rcc_test_2");

  ret = CMEM_GFPBPASegmentAllocate(size1, name1, &handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  printf("First CMEM_BPASegmentAllocate returns handle = %d\n",handle);

  ret = CMEM_GFPBPASegmentAllocate(size2, name2, &handle2);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  printf("Second CMEM_BPASegmentAllocate returns handle = %d\n",handle2);
  
  ret = CMEM_SegmentVirtualAddress(handle, &uaddr);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(handle, &paddr);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentSize(handle, &asize);
  if (ret)
    rcc_error_print(stdout, ret);
 
  //Write some data to the first buffer
  u_int *mptr = (u_int *) uaddr;
  for(u_int loop2 = 0; loop2 < 10; loop2++)
    *mptr++ = 0x11223300 + loop2;

  mptr = (u_int *) uaddr;
  printf("Test data = 0x%08x at 0x%16lx\n", *mptr, (u_long)mptr);

  printf("First segment:\n");
  printf("Physical address = 0x%016lx\n", paddr);
  printf("Virtual address  = 0x%016lx\n", uaddr);
  printf("Actual size      = 0x%016lx\n", asize);
  
  ret = CMEM_SegmentVirtualAddress(handle2, &uaddr);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(handle2, &paddr);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentSize(handle2, &asize);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("Second segment:\n");
  printf("Physical address = 0x%016lx\n", paddr);
  printf("Virtual address  = 0x%016lx\n", uaddr);
  printf("Actual size      = 0x%016lx\n", asize);
  
  //test the entire buffer
  ptr = (u_int *)uaddr;
  for(loop = 0; loop < (size2 >> 2); loop++)
  {
    *ptr = loop;
    if (loop != *ptr)
      printf("ERROR: Failed to write to virt. addr 0x%016lx\n", (u_long)ptr); 
    ptr++;
  }

  CMEM_Dump();
  printf("Press <return> to release the buffer\n");
  fgets(dummy, 10, stdin);
     
  ret = CMEM_GFPBPASegmentFree(handle);
  if (ret)
    rcc_error_print(stdout, ret);
  printf("handle %d returned\n", handle);
     
  ret = CMEM_GFPBPASegmentFree(handle2);
  if (ret)
    rcc_error_print(stdout, ret);
  printf("handle %d returned\n", handle2);
  
  ret = CMEM_Close();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  return(0);
}
