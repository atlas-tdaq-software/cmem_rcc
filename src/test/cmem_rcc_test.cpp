/****************************************************************/
/*								*/
/*  This is the test program for the CMEM_RCC driver & library	*/
/*								*/
/*  12. Dec. 01  MAJO  created					*/
/*								*/
/***********C 2019 - A nickel program worth a dime***************/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"

int main(void)
{
  u_long asize, size = 0x1000, paddr, uaddr;
  u_int ret, loop, dblevel = 0, dbpackage = DFDB_CMEMRCC, *ptr;
  int handle2, handle;
  char dummy[20], name[CMEM_MAX_NAME];

  printf("Enter the debug level: ");
  dblevel = getdecd(dblevel);
  printf("Enter the debug package: ");
  dbpackage = getdecd(dbpackage);
  DF::GlobalDebugSettings::setup(dblevel, dbpackage);
  
  printf("ioctl codes:\n");  
  printf("CMEM_RCC_GET       = 0x%016lx\n", CMEM_RCC_GET);
  printf("CMEM_RCC_FREE      = 0x%016lx\n", CMEM_RCC_FREE);
  printf("CMEM_RCC_LOCK      = 0x%016lx\n", CMEM_RCC_LOCK);
  printf("CMEM_RCC_UNLOCK    = 0x%016lx\n", CMEM_RCC_UNLOCK);
  printf("CMEM_RCC_GETPARAMS = 0x%016lx\n", CMEM_RCC_GETPARAMS);
  printf("CMEM_RCC_SETUADDR  = 0x%016lx\n", CMEM_RCC_SETUADDR);
  printf("CMEM_RCC_DUMP      = 0x%016lx\n", (u_long)CMEM_RCC_DUMP);
  
  ret = CMEM_Open();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  
  printf("Enter the size [in bytes] of the buffers to be allocated: ");
  size = gethexd(size);
  
  printf("Enter the name of the buffer: ");
  getstrd(name, (char *)"cmem_rcc_test");

  ret = CMEM_SegmentAllocate(size, name, &handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  printf("First CMEM_SegmentAllocate returns handle = %d\n",handle);

  ret = CMEM_SegmentAllocate(size, name, &handle2);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  printf("Second CMEM_SegmentAllocate returns handle = %d\n",handle2);
  
  ret = CMEM_SegmentVirtualAddress(handle, &uaddr);
  if (ret)
    rcc_error_print(stdout, ret);
  
  //Write some data to the first buffer  
  u_int *mptr = (u_int *) uaddr;
  for(u_int loop2 = 0; loop2 < 10; loop2++)
    *mptr++ = 0x11223300 + loop2; 

  mptr = (u_int *) uaddr;
  printf("Test data = 0x%08x\n", *mptr);

  ret = CMEM_SegmentPhysicalAddress(handle, &paddr);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentSize(handle, &asize);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("First segment:\n");
  printf("Physical address = 0x%016lx\n", paddr);
  printf("Virtual address  = 0x%016lx\n", uaddr);
  printf("Actual size      = 0x%016lx\n", asize);
  
  ret = CMEM_SegmentVirtualAddress(handle2, &uaddr);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentPhysicalAddress(handle2, &paddr);
  if (ret)
    rcc_error_print(stdout, ret);
  
  ret = CMEM_SegmentSize(handle2, &asize);
  if (ret)
    rcc_error_print(stdout, ret);
 
  printf("Second segment:\n");
  printf("Physical address = 0x%16lx\n", paddr);
  printf("Virtual address  = 0x%16lx\n", uaddr);
  printf("Actual size      = 0x%16lx\n", asize);
  
  //test the entire buffer
  ptr = (u_int *)uaddr;
  for(loop = 0; loop < (size >> 2); loop++)
  {
    *ptr = loop;
    if (loop != *ptr)
      printf("ERROR: Failed to write to virt. addr 0x%016lx\n", (u_long)ptr); 
    ptr++;
  }

  CMEM_Dump();
  printf("Press <return> to release the buffer\n");
  fgets(dummy, 10, stdin);
     
  ret = CMEM_SegmentFree(handle);
  if (ret)
    rcc_error_print(stdout, ret);
  printf("handle %d returned\n",handle);
     
  ret = CMEM_SegmentFree(handle2);
  if (ret)
    rcc_error_print(stdout, ret);
  printf("handle %d returned\n",handle2);

  ret = CMEM_Close();
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(-1);
  }
  return(0);
}
