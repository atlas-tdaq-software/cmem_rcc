
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <sys/ioctl.h>
#include "rcc_error/rcc_error.h"
#include "cmem_rcc/cmem_rcc.h"
#include "DFDebug/DFDebug.h"
#include "ROSGetInput/get_input.h"

// Define here the type of test
#define PHYSMEM_TEST
// #define MEM_TEST

typedef unsigned long long wordType;

///#define MAX_SIZE_B ((unsigned long long)4 * 1024 * 1024 * 1024)
#define MAX_SIZE_B ((unsigned long long)128 * 1024 * 1024)
#define MAX_SIZE_W (MAX_SIZE_B / sizeof(wordType))

///long long testSizes[] = { 1024, MAX_SIZE_W, -1 };
long long testSizes[] = { 1024, 2048, MAX_SIZE_W / 2, -1};
long long maxTestSize = { (long long)32.0 * (long long)1024.0 * (long long)1024.0 };
struct timeval now, start, end;
volatile void *memPtr = NULL;

#define TEST_WARMUP        2
#define TEST_MIN_DURATION  2000000
#define TEST_MIN_LOOPS     2

#ifndef TRUE
# define TRUE (0 == 0)
#endif

#ifndef FALSE
# define FALSE (0 == 1)
#endif

#define SP(l) l,sizeof(l)
#define AP(l) &l[strlen(l)],sizeof(l)-strlen(l)

// ===========================================================
#ifdef PHYSMEM_TEST
int physmemFd = -1;
unsigned int ret;
int handle;
unsigned long uaddr;
#endif
#ifdef MEM_TEST
wordType memBlock[MAX_SIZE_W];
#endif 


inline void openMem() 
{
  if (memPtr != NULL) 
    fprintf(stderr, "openMem called with memPtr non-NULL (ignored)\n");
  
#ifdef PHYSMEM_TEST
  if (physmemFd != -1) 
    fprintf(stderr, "openMem called with PHYSMEM already opened (ignored)\n");
  
  if (physmemFd == -1) 
  {
    ret = CMEM_Open();
    if (ret)
    {
      rcc_error_print(stdout, ret);
      exit(-1);
    }    
    physmemFd = 1;
  }
  ///if ((physmemFd = open("/dev/physmem1", O_RDWR)) == -1) 
  ///{
  ///  perror("Failed to open physmem");
  ///  exit(1);
  ///}

  ///long physmemBytes = 0;
  ///int ioctlStat = 0;
  ///#define PHYSMEM_GETSIZE  _IOR('x', 2, u_long)
  ret = CMEM_GFPBPASegmentAllocate(MAX_SIZE_B, (char *)"Roberto", &handle);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    CMEM_Close();
    exit(-1);
  }
  
  ///if((ioctlStat = ioctl(physmemFd, PHYSMEM_GETSIZE, &physmemBytes)) < 0 ) 
  ///{
  ///  perror("Failed to get PHYSMEM size");
  ///  exit(1);
  ///}

  ///if (physmemBytes < MAX_SIZE_B + 4) 
  ///{
  ///  fprintf(stderr, "PHYSMEM too small needed:%lld got:%ld\n", MAX_SIZE_B+4, physmemBytes);
  ///  exit(1);
  ///}

  ret = CMEM_SegmentVirtualAddress(handle, &uaddr);
  if (ret)
  {
    rcc_error_print(stdout, ret);
    exit(1);
  }
  memPtr = (volatile void *)uaddr;
    
  ///if ((memPtr = mmap(0, MAX_SIZE_B, PROT_READ|PROT_WRITE, MAP_SHARED, physmemFd, 0)) == MAP_FAILED) 
  ///{
  ///  perror("Failed to map physmem");
  ///  exit(1);
  ///}
  return;
#endif

#ifdef MEM_TEST
  memPtr = &memBlock;
  return;
#endif
  fprintf(stderr, "openMem: please define the test type\n");
  exit(1);
}


/********************/
inline void closeMem() 
/********************/
{
  if (memPtr == NULL) 
    fprintf(stderr, "closeMem called with memPtr NULL (ignored)\n");

#ifdef PHYSMEM_TEST
  if (memPtr != NULL) 
  {
    ret = CMEM_GFPBPASegmentFree(handle);
    if (ret)
    {
      rcc_error_print(stdout, ret);
      exit(1);
    }
    ///  if (munmap((void *)memPtr, MAX_SIZE_B))
    ///    perror("Error during PHYSMEM unmap (ignored)");
    memPtr = NULL;
  }
  
  if (physmemFd != -1) 
  {  
    ret = CMEM_Close();
    if (ret)
    {
      rcc_error_print(stdout, ret);
      exit(-1);
    }    
    ///close(physmemFd);
    physmemFd = -1;
  } 
  else 
    fprintf(stderr, "closeMem called with physemFd not set (ignored)\n");
  return;
#endif
#ifdef MEM_TEST
  memPtr = NULL;
  return;
#endif
  fprintf(stderr, "closeMem: please define the test type\n");
  exit(1);
}


/***************************************/
inline void getNow(struct timeval *nowIn) 
/***************************************/
{
  if (nowIn == NULL)
    gettimeofday(&now, NULL);
  else
    gettimeofday(nowIn, NULL);
}


// Calculate delta time in usec
/********************************************************************/
inline long long deltaTime(struct timeval *end, struct timeval *start) 
/********************************************************************/
{
  return((end->tv_sec - start->tv_sec) * 1000000 + (end->tv_usec - start->tv_usec));
} 


// Dump delta time in ASCII format
/******************************/
char *dumpTime(double deltaTime) 
/******************************/
{
  static char o[10][1024];
  static int  n = 0;
  char s[1024];
  
  if (deltaTime > 10) 
    snprintf(SP(s), "%lld", (long long) deltaTime);
  else 
    snprintf(SP(s), "%.2e", deltaTime);

  snprintf(AP(s), " usec");
  const int oneSec = 1000000;
  if (deltaTime >= oneSec) 
    snprintf(AP(s), " (%.2f sec)", (double)deltaTime / oneSec);

  const int m = n;
  strncpy(o[m], s, sizeof(s));
  n = (n + 1) % 10;
  return(o[m]);
}



// Measure the time to open/close the device
/****************/
void measureOpen() 
/****************/
{
  long long nLoops, elapsed;

  for (nLoops = 0; ++nLoops != TEST_WARMUP;) 
  {
    openMem();
    closeMem();
  }

  for (nLoops = 1, getNow(&start); ; nLoops++) 
  {
    openMem();
    closeMem();
    getNow(&end);
    if ((elapsed = deltaTime(&end, &start)) > TEST_MIN_DURATION) break;
  }
  printf("Open/map/close:%s elapsed:%s loops:%lld\n", dumpTime((double)elapsed / nLoops), dumpTime(elapsed), nLoops);
} 


/****************/
void measureRead() 
/****************/
{
  long long b;
  wordType dummyIn;
  volatile wordType *memIn;
  int s;

  printf("benchmarking reads\n");

  for (s = 0; testSizes[s] != -1; s++) 
  {
    long long elapsed;
    int nLoops;
    long long thisTestSize = testSizes[s];
    static int lastTestSize = -1;
    if (thisTestSize > maxTestSize) thisTestSize = maxTestSize;
    if (thisTestSize == lastTestSize) continue;
    lastTestSize = thisTestSize;
  
    printf("thisTestSize = %llu\n", thisTestSize);

    for (nLoops = 0; nLoops != TEST_WARMUP; nLoops++)
    {
      for (memIn = (volatile wordType *)memPtr, dummyIn = 0, b = 0; b != thisTestSize; b++) 
        dummyIn |= *memIn++;
    }
    
    for (nLoops = 1, getNow(&start); ; nLoops++) 
    {
      for (memIn = (volatile wordType *)memPtr, dummyIn = 0, b = 0; b != thisTestSize; b++) 
        dummyIn |= *memIn++;      
      
      getNow(&end);
      if ((elapsed = deltaTime(&end, &start)) > TEST_MIN_DURATION
	&& nLoops >= TEST_MIN_LOOPS) break;
    }
    
    thisTestSize *= sizeof(wordType);
    const double perLoop = (double)elapsed / nLoops;
    const double perByte = perLoop / thisTestSize;
    printf("Read  size:%-16lld", thisTestSize);
    printf(" time:%-30s", dumpTime(perLoop));
    printf(" perByte:%s", dumpTime(perByte));
    printf("      (loops:%d time:%s)", nLoops, dumpTime(elapsed));
    printf("\n");
  }
} 


void measureWrite() 
{
  wordType wordOut = 0xdeadbeef;
  long long b;
  volatile wordType *memOut;
  int s = 0;

  printf("benchmarking writes\n");

  for (s = 0; testSizes[s] != -1; s++) 
  {
    long long elapsed;
    int nLoops;
    long long thisTestSize = testSizes[s];
    static int lastTestSize = -1;
    if (thisTestSize > maxTestSize) thisTestSize = maxTestSize;
    if (thisTestSize == lastTestSize) continue;
    lastTestSize = thisTestSize;

    printf("thisTestSize = %llu\n", thisTestSize);

    for (nLoops = 0; nLoops != TEST_WARMUP; nLoops++)
    {
      memOut = (volatile wordType *)memPtr;
      for (b = 0; b != thisTestSize; b++) 
        *memOut++ = wordOut;
    }
    
    for (nLoops = 0, getNow(&start);;) 
    {
      nLoops++;
      
      memOut = (volatile wordType *)memPtr;
      for (b = 0; b != thisTestSize; b++) 
        *memOut++ = wordOut;
      
      getNow(&end);
      if ((elapsed = deltaTime(&end, &start)) > TEST_MIN_DURATION
	&& nLoops >= TEST_MIN_LOOPS) break;
    }
    
    
    thisTestSize *= sizeof(wordType);
    const double perLoop = (double)elapsed/nLoops;
    const double perByte = perLoop / thisTestSize;
    printf("Write size:%-16lld", thisTestSize);
    printf(" time:%-30s", dumpTime(perLoop));
    printf(" perByte:%s", dumpTime(perByte));
    printf("      (loops:%d time:%s)", nLoops, dumpTime(elapsed));
    printf("\n");
  }
} 



const char *dumpSize(long long s, char suffix) 
{
  static int i=0;
  static char outStr[ 10 ][ 1024 ];
  static char outVal[ 1024 ];

  snprintf(SP(outVal), "%lld %c", s, suffix);
  if (s < 1024) 
  {
  } 
  else if ((double)s < 1024 * 1024) 
    snprintf(AP(outVal), " = %.0f K%c", (double)s/1024.0, suffix);
  else if ((double)s < 1024 * 1024 * 1024) 
    snprintf(AP(outVal), " = %.0f M%c", (double)s/(1024.0*1024.0), suffix);
  else if ((double)s < (double)(1024.0 * 1024 * 1024 * 1024)) 
    snprintf(AP(outVal), " = %.0f G%c", (double)s/(1024.0 * 1024.0 * 1024.0), suffix);
  else 
    snprintf(AP(outVal), " = %.0f T%c", (double)s/(1024.0 * 1024.0 * 1024.0 * 1024.0), suffix);
  
  i = (i + 1) % 10;
  strncpy(outStr[i], outVal, sizeof(outVal));
  return(outStr[i]);
}


inline void measurePipe() 
{
  long long elapsed;
  int nLoops;

  ///doTest(TRUE);
  for (nLoops = 1, getNow(&start); ; nLoops++) 
  {
    FILE *fd = popen("true", "r");
    while (!feof(fd)) 
      fgetc(fd);

    pclose(fd);
    getNow(&end);
    if (nLoops >= TEST_WARMUP) break;
  }
    
  ///doTest(FALSE);
  for (nLoops = 1, getNow(&start); ; nLoops++) 
  {
    FILE *fd = popen("true", "r");
    while (!feof(fd)) 
      fgetc(fd);

    pclose(fd);
    getNow(&end);
    if ((elapsed = deltaTime(&end, &start)) > TEST_MIN_DURATION) break;
  }
  
  double resultMapped = elapsed / nLoops;

  openMem();
  ///doTest(TRUE);
  for (nLoops = 1, getNow(&start); ; nLoops++) 
  {
    FILE *fd = popen("true", "r");
    while (!feof(fd)) 
      fgetc(fd);

    pclose(fd);
    getNow(&end);
    if (nLoops >= TEST_WARMUP) break;
  }
    
  ///doTest(FALSE);
  for (nLoops = 1, getNow(&start); ; nLoops++) 
  {
    FILE *fd = popen("true", "r");
    while (!feof(fd)) 
      fgetc(fd);

    pclose(fd);
    getNow(&end);
    if ((elapsed = deltaTime(&end, &start)) > TEST_MIN_DURATION) break;
  }
  
  
  closeMem();
  double resultUnmapped = elapsed / nLoops;

  printf("Pipe");
  printf(" unmapped:%s", dumpTime(resultMapped));
  printf(" mapped:%s", dumpTime(resultUnmapped));
  printf("\n");
} 



int main(int argc, char **argv) 
{
  printf("%s starting\n", argv[0]);
  printf("Test type: ");
#ifdef PHYSMEM_TEST
  printf("\tPHYSMEM");
#else
# ifdef MEM_TEST
  printf("\tPROCESS-MEMORY");
# else
  printf("\tUNKNOWN!!!");
# endif
#endif
  printf("\n");
  printf("\tWord size:%lu", sizeof(wordType));
  printf("\tMapped size %s, %s", dumpSize(MAX_SIZE_B, 'B'), dumpSize(MAX_SIZE_W, 'W'));
  printf("\tMax test size:%s, %s\n", dumpSize(maxTestSize * sizeof(wordType), 'B'), dumpSize(maxTestSize, 'W'));
  printf("\tTest sizes:\n");
  int s;
  for (s = 0; testSizes[s] != -1; s++) 
  {
    long long actualSize = testSizes[s];
    if (actualSize > maxTestSize) actualSize = maxTestSize;
    printf("\t\t%-30s \t%-30s\n", dumpSize((double)actualSize * sizeof(wordType), 'B'), dumpSize((double)actualSize, 'W'));
  }
   
  printf("measureOpen\n");
  measureOpen();

  printf("measurePipe\n");
  measurePipe();

  printf("openMem\n");
  openMem();

  printf("measureRead\n");
  measureRead();

  printf("measureWrite\n");
  measureWrite();

  exit(0);
} 






//Results
/*
Host: pc-tbed-ros-03.cern.ch
OS  : Linux pc-tbed-ros-03.cern.ch 2.6.18-348.1.1.el5 #1 SMP Thu Jan 24 13:27:00 CET 2013 i686 i686 i386 GNU/Linux
CPU : Intel(R) Xeon(TM) CPU 3.40GHz

Test type:      PHYSMEM
        Word size:8     Mapped size 134217728 B = 128 MB, 16777216 W = 16 MW    Max test size:268435456 B = 256 MB, 33554432 W = 32 MW
        Test sizes:
                8192 B = 8 KB                   1024 W = 1 KW
                16384 B = 16 KB                 2048 W = 2 KW
                67108864 B = 64 MB              8388608 W = 8 MW
Open/map/close:3171 usec elapsed:2000997 usec (2.00 sec) loops:631
Pipe unmapped:4830 usec mapped:6004 usec
benchmarking reads
Read  size:8192             time:6.53e+00 usec                  perByte:7.98e-04 usec      (loops:306089 time:2000001 usec (2.00 sec))
Read  size:16384            time:12 usec                        perByte:7.90e-04 usec      (loops:154496 time:2000008 usec (2.00 sec))
Read  size:67108864         time:53463 usec                     perByte:7.97e-04 usec      (loops:38 time:2031622 usec (2.03 sec))
benchmarking writes
Write size:8192             time:5.97e+00 usec                  perByte:7.29e-04 usec      (loops:335014 time:2000003 usec (2.00 sec))
Write size:16384            time:11 usec                        perByte:7.01e-04 usec      (loops:174237 time:2000005 usec (2.00 sec))
Write size:67108864         time:48982 usec                     perByte:7.30e-04 usec      (loops:41 time:2008272 usec (2.01 sec))


Host: pc-tbed-ros-02.cern.ch
OS  : Linux pc-tbed-ros-02.cern.ch 2.6.32-279.slc6.nonpae.i686 #1 SMP Fri Aug 10 08:51:03 CEST 2012 i686 i686 i386 GNU/Linux
CPU : Intel(R) Core(TM)2 Quad CPU    Q9650  @ 3.00GHz

Test type:      PHYSMEM
        Word size:8     Mapped size 134217728 B = 128 MB, 16777216 W = 16 MW    Max test size:268435456 B = 256 MB, 33554432 W = 32 MW
        Test sizes:
                8192 B = 8 KB                   1024 W = 1 KW
                16384 B = 16 KB                 2048 W = 2 KW
                67108864 B = 64 MB              8388608 W = 8 MW
Open/map/close:9250 usec elapsed:2007395 usec (2.01 sec) loops:217
Pipe unmapped:3026 usec mapped:11492 usec
benchmarking reads
Read  size:8192             time:5.06e+00 usec                  perByte:6.18e-04 usec      (loops:395310 time:2000005 usec (2.00 sec))
Read  size:16384            time:9.85e+00 usec                  perByte:6.01e-04 usec      (loops:203128 time:2000010 usec (2.00 sec))
Read  size:67108864         time:40483 usec                     perByte:6.03e-04 usec      (loops:50 time:2024156 usec (2.02 sec))
benchmarking writes
Write size:8192             time:4.53e+00 usec                  perByte:5.53e-04 usec      (loops:441531 time:2000004 usec (2.00 sec))
Write size:16384            time:8.69e+00 usec                  perByte:5.30e-04 usec      (loops:230159 time:2000004 usec (2.00 sec))
Write size:67108864         time:36824 usec                     perByte:5.49e-04 usec      (loops:55 time:2025354 usec (2.03 sec))
*/











